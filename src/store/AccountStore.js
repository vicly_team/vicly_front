import {computed, observable, action, runInAction} from "mobx";
import {BACKEND_URL} from "../common";

export default class AccountStore {
    @observable fullName = "";
    first_name = "";
    last_name = "";
    @observable token = "";
    @observable login = "";
    position = "";
    userId = null;
    groupId = null;
    @observable status = "unauthed";
    err_message = "";
    theme = 'light';
     online = [];

    constructor(RootStore) {
        this.fullName = sessionStorage.getItem("fullName");
        this.position = sessionStorage.getItem("position");
        this.first_name = sessionStorage.getItem("first_name");
        this.surname = sessionStorage.getItem("surname");
        this.last_name = sessionStorage.getItem("last_name");
        this.token = sessionStorage.getItem("token");
        this.userId = parseInt(sessionStorage.getItem("userId"), 10);
        this.groupId = parseInt(sessionStorage.getItem("groupId"), 10);
        this.login = sessionStorage.getItem("login");
        let theme = sessionStorage.getItem("theme");
        if (theme) {
            this.theme = theme;
        } else {
            sessionStorage.setItem("theme", this.theme);
        }
        if (this.token) {
            this.status = "authed";
        }
        this.rootStore = RootStore;

    }

    start(){
        if(this.token)
            this.rootStore.webSocketService.run(this.token);
    }

    async loginUser(login, password) {
        try {
            const response = await fetch(BACKEND_URL + "/user/login", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "login": login,
                    "password": password
                })
            });
            if (!response.ok) {
                runInAction("Auth failed", () => {
                    this.status = "failed";
                    this.err_message = response.error();
                });
            }
            const content = await response.json();

            runInAction("Auth success", () => {
                this.fullName = content.first_name + " " + content.last_name;
                this.first_name = content.first_name;
                this.last_name = content.last_name;
                this.position = content.position;
                this.token = content.token;
                this.login = login;
                this.status = "authed";
                this.userId = content.id;
                this.groupId = content.group_id;
                this.saveInLocalStorage(this.fullName,this.first_name, this.last_name, this.token, this.userId, this.groupId, this.login, this.position);
            });
            this.rootStore.webSocketService.run();
        } catch (err) {
            console.log(err);
            runInAction("Auth failed", () => {
                this.status = "failed";
                this.err_message = err;
            });
        }
    };

    saveInLocalStorage(fullName,first_name, last_name, token, userId, groupId, login, position) {
        sessionStorage.setItem("fullName", fullName);
        sessionStorage.setItem("first_name", first_name);
        sessionStorage.setItem("last_name", last_name);
        sessionStorage.setItem("token", token);
        sessionStorage.setItem("userId", userId);
        sessionStorage.setItem("groupId",groupId);
        sessionStorage.setItem("login",login);
        sessionStorage.setItem("position",position ? position : '');
    }

    @action
    unauth() {
        this.fullName = null;
        this.token = null;
        this.login = null;
        this.status = "unauthed";
        sessionStorage.clear();
    }

    setTheme (isLight) {
        sessionStorage.setItem("theme", isLight ? 'light' : 'dark');
    }

    showOnline(onlineId) {
        let count = 0;

        for (let i = 0; i < this.online.length; i++) {
            if (this.online[i] === onlineId) {
                count++
            }
        }
        if (count === 0) {
            this.online.push(onlineId)
        }
        };

    showOffline(onlineId) {
        for (let i = 0; i < this.online.length; i++) {
            if (this.online[i] === onlineId) {
                this.online.splice(i, 1)
            }
        }
    };
}
