import React from 'react';
import {Badge, IconButton, withStyles} from "@material-ui/core";
import ProfileIco from "./ProfileIco";
import Typography from "@material-ui/core/es/Typography/Typography";
import InviteIcon from "./InviteIcon";
import ExitToApp from '@material-ui/icons/ExitToApp';
import rootStore from "../store/RootStore";
import "animate.css/animate.min.css"
import DownloadIcon from "./DownloadIcon";

const {accountStore} = rootStore;

const styles = theme => ({
    position: {
        zIndex: 1,
        borderRadius: '5px 5px 0 0',
        margin: '5px 10px 0px 8px',
        position: 'absolute',
        backgroundColor: ` ${
            theme.palette.type === 'light' ? theme.palette.primary.light : '#2b3346'
        }`,
        display: 'inline-flex',
        alignItems: 'center',
        top: 0,
        left: 0,
        right: 0,

    },
    userDisplay: {},
    bage: {
        backgroundColor: '#41ff9c',
        minWidth: 8,
        height: 8,
        color: '#fff',
        left: 42,
        top: 11,
        width: 8,
    },
    text: {
        color: ` ${
            theme.palette.type === 'light' ? 'rgb(140, 140, 140)' : 'rgb(159, 171, 199)'
        }`,
    },
    marginInvite: {
        marginLeft: 'auto',
    },
    wrap: {
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        width: '100%',
    },
    online: {
        color: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.light : theme.palette.secondary.dark
        }`,
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
    },
    barDisp: {
        display: 'inline-flex',
    },
    butt: {
        marginLeft: 'auto',
    },
    name: {
        marginTop: 7,
    },
    icon: {
        color: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.lightIcons : theme.palette.secondary.dark
        }`,
    },
});


class ProfileBar extends React.Component {
    constructor(props) {
        super(props);
        this.accountStore = accountStore;
        this.handleLogoutFunc = this.accountStore.unauth.bind(accountStore);
    }

    render() {
        const {classes} = this.props;
        return (
            <div className={classes.position + ' animated fadeInDown faster'}>
                <ProfileIco
                    changeThemeType={this.props.changeThemeType}
                    handleLogout={this.handleLogoutFunc}
                    name={this.accountStore.fullName}/>

                <div className={classes.wrap}>
                    <Typography variant="h6" className={classes.online}>{accountStore.fullName}</Typography>
                    <Badge classes={{badge: classes.bage}}>
                        <Typography className={classes.text}> online </Typography>
                    </Badge>
                </div>

                <InviteIcon/>

                <IconButton onClick={this.handleLogoutFunc}>
                    <ExitToApp className={classes.icon}/>
                </IconButton>
            </div>
        );
    }
}

export default withStyles(styles, {withTheme: true, index: 1})(ProfileBar);