import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import Dialog from "./ChatUser/Dialog";
import ListItem from '@material-ui/core/ListItem';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Typography from "@material-ui/core/Typography";
import rootStore from "../store/RootStore";
import {observer} from "mobx-react";
import Loyalty from "@material-ui/icons/Loyalty"
import deferComponentRender from "./DeferredWrapper";
import "animate.css/animate.min.css"
import GroupChat from "./ChatGroup/GroupChat";

const {accountStore, messagesStore} = rootStore;


const styles = theme => ({
    groupName: {
        paddingTop: 0,
        paddingBottom: 0,
        padding: '6px 0 6px 0'
    },
    text: {
        color: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.light : theme.palette.secondary.dark
        }`,
        fontSize: '0.9rem'
    },
    icon: {
        color: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.light : theme.palette.secondary.dark
        }`,
        marginLeft: 'auto',
        padding: 9,
    },
    root: {

        backgroundColor: ` ${
            theme.palette.type === 'light' ? '#e6e6e6' : '#40485d'
        }`,
    },

    badge: {
        marginLeft: 10,
    },
    workgroupName: {
        display: 'flex',
        alignItems: 'center',
        padding: 8,
    },
    WorkGroupBack: {

        /*margin: '8px 3px 0px 8px',*/
        [theme.breakpoints.down('xs')]: {
            /*   margin: '8px 8px 10px 8px',*/
        },
        borderRadius: 5,
        // paddingBottom: 5,
        marginBottom: 10,
        boxShadow: ` ${
            theme.palette.type === 'light' ? 'inset 0px -3px 0px 0px rgb(213, 213, 213)' : 'inset 0px -4px 0px 0px rgba(19, 24, 37, 0.8)'
        }`,
        backgroundColor: ` ${
            theme.palette.type === 'light' ? '#fff' : '#2b3346'
        }`,

    },
});

@observer
class Workgroup extends React.Component {
    state = {
        open: true,
    };

    handleClick = () => {
        this.setState(state => ({open: !state.open}));
    };

    componentDidCatch(error, info) {
        // You can also log the error to an error reporting service
    }

    workGroupColor = (letter) => {
        let col = this.colorMap[letter];
        if (col) return col;
    };

    colorMap = {
        "t": "#009bed",
        "Б": "#6aa8b4",
        "И": "#9e72cf"

    };

    render() {
        const {classes, theme, workgroup, groupChats, userChats, messagesStore, userChatsNew, groupChatsNew} = this.props;
        const lol = workgroup.name;
        let wcolor = lol.charAt(0);
        let colorName = this.workGroupColor(wcolor);

        return (
            <div className={classes.WorkGroupBack +' animated fadeInDown faster'}>
                <ListItem disableGutters button onClick={this.handleClick} className={classes.groupName}>
                        <div className={classes.workgroupName}>
                            <Typography variant='button' className={classes.text}>
                                {workgroup.name}
                            </Typography>
                            <Loyalty style={{color: `${colorName}`}} className={classes.badge}/>
                        </div>
                    {this.state.open ? <ExpandLess className={classes.icon}/> : <ExpandMore className={classes.icon}/>}
                </ListItem>
                <Collapse in={this.state.open} timeout="auto">
                    <List disablePadding className={classes.active}>
                        {
                            userChatsNew.map(
                                userChat =>
                                    <Dialog
                                        key={userChat.user.id}
                                        userChat={userChat}
                                        handleDrawerToggleForMob={this.props.handleDrawerToggleForMob}/>
                            )
                        }
                        {
                            groupChatsNew.map(
                                groupChat =>
                                    <GroupChat
                                        key={groupChat.chatId}
                                        groupChat={groupChat}
                                        handleDrawerToggleForMob={this.props.handleDrawerToggleForMob}/>
                            )
                        }
                    </List>
                </Collapse>
            </div>
        )
    }
}

export default deferComponentRender(withStyles(styles, {withTheme: true, index: 1})(Workgroup));