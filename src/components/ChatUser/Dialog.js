import React from 'react';
import Avatar from '@material-ui/core/Avatar/index';
import Typography from '@material-ui/core/Typography/index';
import 'typeface-roboto';
import ListItem from "@material-ui/core/ListItem/ListItem";
import withStyles from "@material-ui/core/es/styles/withStyles";
import Badge from "@material-ui/core/Badge/Badge";
import {observer} from "mobx-react";
import rootStore from "../../store/RootStore";
import AvatarColor from "../../services/AvatarColor"
import history from "../../store/history";

const {accountStore, messagesStore} = rootStore;

const styles = theme => ({
    fixWidth: {
        margin: 0,
        //width: 'inherit',
        width: '100%',
        display: 'flex',
    },
    avatar: {
        width: 45,
        height: 45,
        borderRadius: 5,
    },
    listItemPadding: {
        padding: 'unset',
        borderRadius: 5,
    },
    margin: {
        top: 41,
        right: 17,
        backgroundColor: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.lightBadge : theme.palette.secondary.dark
        }`,
        color: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.dark : theme.palette.secondary.light
        }`,
        zIndex: 0,
    },
    fixPadding: {
        padding: 0,
        margin: 0,
    },
    contentPadding: {
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 9,
    },
    userName: {
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        width: '100%',
        fontSize: '1rem',
        fontWeight: '400',
        color: ` ${
            theme.palette.type === 'light' ? '#585858' : theme.palette.secondary.dark
        }`,
    },
    message: {
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
        width: '100%',
        fontWeight: 400,
        color: ` ${
            theme.palette.type === 'light' ? '#adacac' : theme.palette.secondary.dark
        }`,
        fontSize: '0.9rem'
    },
    time: {
        fontSize: '0.9rem',
        color: ` ${
            theme.palette.type === 'light' ? '#adacac' : theme.palette.secondary.dark
        }`,
    },
    onlineNotSelected: {
        backgroundColor: '#66ff80',
        zIndex: 0,
        top: 37,
        right: 4,
        border: ` ${
            theme.palette.type === 'light' ? '3px solid #fff' : '3px solid #2b3346'
        }`,
    },
    onlineSelected: {
        backgroundColor: '#66ff80',
        zIndex: 0,
        top: 35,
        right: 6,
        border: '3px solid #2d807f',
    },
});

@observer
class Dialog extends React.Component {

    constructor(props) {
        super(props);
        this.messagesStore = messagesStore;
        this.accountStore = accountStore;
    }

    handleDialogClick = () => {
        history.push(`/home/chat/user/${this.props.userChat.user.id}`);
        this.props.handleDrawerToggleForMob();
    };

    handleDialogClickMob = () => {
        this.props.history.push(`/home/chat/user/${this.props.userChat.user.id}`);
        // FIXME comment is fix for url chat page reload dafauck mafuck
        this.props.handleDrawerToggle();
    };

    formatDate = (timestamp) => {
        const now = new Date(Date.now());
        let date = new Date(timestamp);
        const today = now.toDateString() == date.toDateString();
        const mins = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        if (today) {
            return date.getHours() + ":" + mins;
        } else {
            //  return date.getHours() + ":" + mins + " " + date.getDay() + "/" + date.getMonth() + "/" + (date.getFullYear() - 2000);
            return date.getDay() + "/" + date.getMonth() + "/" + (date.getFullYear() - 2000);
        }
    };

    render() {
        const {classes, userChat} = this.props;
        const userId = userChat.user.id;
        const firstName = userChat.user.first_name;
        const lastName = userChat.user.last_name;
        const lastMessage = userChat.last ? userChat.last : null;
        const countUnread = userChat.unread;
        const lastMessageDatetime = userChat.last ? userChat.last.timestamp_post.timestamp : null;
        const selected = userChat.selected;
        // TODO work ONLY FOR USERS CHATS

        let colorChange = AvatarColor.getColor(firstName[0]);
        let avatar_image = rootStore.imageService.images.find(elem => elem.userId === userId);

        const online = this.accountStore.online.find(elem => elem === userId);

        return (
            <div>
                <ListItem
                    selected={selected}
                    onClick={this.handleDialogClick}
                    disableGutters={true}
                    button
                    className={classes.listItemPadding}>
                    <div className={`${classes.fixWidth} ${selected ? classes.selected : ""}`}>
                        <div style={{padding: 8}}>
                            {
                                avatar_image ?
                                    (
                                        online ? (
                                            <Badge color="secondary"
                                                   classes={{badge: selected ? classes.onlineSelected : classes.onlineNotSelected}}>
                                                <Avatar
                                                    className={classes.avatar}
                                                    // style={{backgroundColor: `${colorChange}`}}
                                                    src={avatar_image.small}/>
                                            </Badge>
                                        ) : (
                                            <Avatar
                                                className={classes.avatar}
                                                // style={{backgroundColor: `${colorChange}`}}
                                                src={avatar_image.small}/>
                                        )
                                    ) : (
                                        online ? (
                                            <Badge color="secondary"
                                                   classes={{badge: selected ? classes.onlineSelected : classes.onlineNotSelected}}>
                                                <Avatar
                                                    className={classes.avatar}
                                                    style={{backgroundColor: `${colorChange}`}}>
                                                    {firstName[0].toUpperCase() + lastName[0].toUpperCase()}
                                                </Avatar>
                                            </Badge>
                                        ) : (
                                            <Avatar
                                                className={classes.avatar}
                                                style={{backgroundColor: `${colorChange}`}}>
                                                {firstName[0].toUpperCase() + lastName[0].toUpperCase()}
                                            </Avatar>
                                        )
                                    )
                            }
                        </div>

                        <div style={{padding: 8, overflow: 'hidden',}}>
                            <Typography variant="h6"
                                        className={classes.userName}
                                        style={{color: selected ? '#fff' : ''}}>{firstName + " " + lastName}</Typography>
                            <Typography variant="h6"
                                        className={classes.message}
                                        style={{color: selected ? '#b5dcdc' : ''}}>{lastMessage ? lastMessage.message : "Нет сообщений"}</Typography>
                        </div>

                        <div style={{padding: 8, margin: '0 0 auto auto'}}>
                            <Typography
                                className={classes.time}
                                style={{color: selected ? '#b5dcdc' : ''}}>
                                {lastMessageDatetime ? this.formatDate(lastMessageDatetime) : ""}
                            </Typography>
                        </div>
                        {
                            countUnread ? (<Badge color="secondary" badgeContent={countUnread}
                                                  classes={{badge: classes.margin}}/>) : ("")
                        }
                    </div>
                </ListItem>
            </div>
        );
    }
}

const styledComponent = withStyles(styles, {withTheme: true, index: 1})(Dialog);

export default styledComponent;
