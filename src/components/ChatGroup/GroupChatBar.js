import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import IconButton from "@material-ui/core/IconButton/IconButton";
import SearchIcon from '@material-ui/icons/Search';
import Typography from "@material-ui/core/Typography/Typography";
import div from "@material-ui/core/Grid/Grid";
import MoreVert from '@material-ui/icons/MoreVert'
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import Menu from "@material-ui/core/Menu/Menu";
import Group from '@material-ui/icons/Group';
import rootStore from "../../store/RootStore";
import InputBase from "@material-ui/core/InputBase";
import UserProfile from "../UserProfile";
import Modal from "@material-ui/core/Modal";
import GroupChatInfo from "./GroupChatInfo";
import AttachmentsModal from "../AttachmentsModal";

const {accountStore, messagesStore} = rootStore;
const top = 50;
const left = 50;

const styles = theme => ({
    paper: {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
        width: 495,
        height: '98%',
        [theme.breakpoints.down('xs')]: {
            position: 'fixed',
            top: 5,
            left: 5,
            right: 5,
            bottom: 5,
            transform: 'none',
            width: 'auto',
        },
        position: 'absolute',
        outline: 'none',
        borderRadius: 10,
        backgroundColor: ` ${
            theme.palette.type === 'light' ? '#0A8D8D' : '#0A8D8D'
        }`,
        boxShadow: theme.shadows[5],
    },
    paperAttachment: {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
        width: 640,
        height: '98%',
        [theme.breakpoints.down('xs')]: {
            position: 'fixed',
            top: 5,
            left: 5,
            right: 5,
            bottom: 5,
            transform: 'none',
            width: 'auto',
        },
        position: 'absolute',
        outline: 'none',
        borderRadius: 10,
        backgroundColor: ` ${
            theme.palette.type === 'light' ? '#0A8D8D' : '#0A8D8D'
        }`,
        boxShadow: theme.shadows[5],
    },
    position: {
        margin: '5px 5px 5px 5px',
        borderRadius: '5px 5px 5px 5px',
        boxShadow: ` ${
            theme.palette.type === 'light' ? 'inset 0px -3px 0px 0px rgb(218, 218, 218), 0px 4px 7px 0px rgba(0, 0, 0, 0.07)' : 'inset 0px -3px 0px 1px rgba(45, 53, 70, 0.86), 0 0 13px 0px rgba(0, 0, 0, 0.21)'
            }`,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        display: 'inline-flex',
        justifyContent: 'space-between',
        // height: 55,
        zIndex: 1,
        backgroundColor: ` ${
            theme.palette.type === 'light' ? theme.palette.primary.light : theme.palette.primary.darkSecondary
            }`,
        [theme.breakpoints.down('xs')]: {
            top: 55,
            borderRadius: '0 0 5px 5px',
        },
        /*borderBottom: ` ${
            theme.palette.type === 'light' ? '1px solid #e6e6e6' : ''
            //  theme.palette.mime === 'light' ? '1px solid #e6e6e6' : '1px solid #40485d'
            }`,*/
        /*  borderLeft: ` ${
              theme.palette.mime === 'light' ? '1px solid #e6e6e6' : ''
              }`,*/

        /*   left: 400,
           [theme.breakpoints.down('md')]: {
               left: 280,
           },
           [theme.breakpoints.down('sm')]: {
               left: 250
           },
           [theme.breakpoints.down('xs')]: {
               left: 0,
               top: 55,
               borderRadius: '0 0 5px 5px',
           },*/
    },
    namePosition: {
        display: 'inline-flex',
        alignItems: 'center',
        maxWidth: '40%'
    },
    searchField: {
        height: 33,
        margin: 7,
        marginRight: 0,
        [theme.breakpoints.down('xs')]: {
            maxWidth: '90%',
        },
        background: '#000!important',
        backgroundColor: '#000!important'
    },
    searchIco: {
        color: theme.palette.secondary.light
    },
    dialogIco: {
        color: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.lightIcons : theme.palette.secondary.dark
            }`,
    },
    text: {
        color: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.light : theme.palette.secondary.dark
            }`,
    },
    search: {
        position: 'relative',
        margin: 10,
    },
    searchIcon: {
        height: '100%',
        width: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingRight: 7,
    },
    inputRoot: {
        color: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.light : theme.palette.secondary.dark
            }`,
    },
    inputInput: {
        backgroundColor: ` ${
            theme.palette.type === 'light' ? '#efefef' : "#49536d"
            }`,
        width: '100%',
        borderRadius: 4,
        padding: 8,
    },
    icon: {
        color: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.lightIcons : theme.palette.secondary.dark
            }`,
    },
    iconSearch: {
        color: ` ${
            theme.palette.type === 'light' ? '#d2d2d2' : 'rgba(255, 255, 255, 0.17)'
            }`,
    },
    menu: {
        backgroundColor: ` ${
            theme.palette.type === 'light' ? '#efefef' : "#49536d"
            }`,
    },
    menuItem: {
        color: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.lightIcons : theme.palette.secondary.dark
            }`,
    },

});

class ChatBar extends React.Component {
    state = {
        auth: true,
        open: false,
        chatInfo: false,
        attachments: false,
        anchorEl: null,
        type: this.props.theme.palette.type,
    };

    constructor(props) {
        super(props);
        this.accountStore = accountStore;
        this.messagesStore = messagesStore;
    }

    handleChange = event => {
        this.setState({auth: event.target.checked});
    };

    handleMenu = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleClose = () => {
        this.setState({anchorEl: null});
    };

    handleMenuClose = () => {
        this.setState({open: false});
        this.setState({attachments: false});
        this.setState({chatInfo: false});
    };

    handleChatInfoOpen = event => {
        this.handleClose();
        this.setState({open: true});
        this.setState({chatInfo: true});
    };

    handleAttachmentsOpen = event => {
        this.handleClose();
        this.setState({open: true});
        this.setState({attachments: true});
    };

    render() {
        const {auth, anchorEl} = this.state;
        const open = Boolean(anchorEl);
        const {classes, theme, match, chat} = this.props;
        const type = this.state.type;

        return (
            <div className={classes.position}>
                <div style={{
                    display: 'inline-flex',
                    alignItems: 'center',
                }}>
                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon className={classes.iconSearch}/>
                        </div>
                        <InputBase
                            placeholder="Поиск сообщений…"
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}/>
                    </div>
                </div>

                <div className={classes.namePosition}>
                    <Typography variant="h6" className={classes.text} noWrap>{chat.title}</Typography>
                    <IconButton>
                        <Group className={classes.dialogIco}/>
                    </IconButton>
                </div>

                <div>
                    <IconButton
                        aria-owns={open ? 'menu-appbar' : undefined}
                        aria-haspopup="true"
                        onClick={this.handleMenu}>
                        <MoreVert className={classes.dialogIco}/>
                    </IconButton>
                    <Menu
                        style={{zIndex: 2000}}
                        id="menu-appbar"
                        anchorEl={anchorEl}
                        anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        open={open}
                        onClose={this.handleClose}
                        classes={{
                            paper: classes.menu,
                        }}>
                        <MenuItem onClick={this.handleChatInfoOpen} className={classes.menuItem}>Информация о чате</MenuItem>
                        <MenuItem onClick={this.handleAttachmentsOpen} className={classes.menuItem}>Вложения</MenuItem>
                        <MenuItem onClick={this.handleClose} className={classes.menuItem}>Заглушить
                            уведомления</MenuItem>
                        <MenuItem onClick={this.handleClose} className={classes.menuItem}>Выйти</MenuItem>
                    </Menu>
                </div>

                <Modal
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    open={this.state.open}
                    onClose={this.handleMenuClose}
                    style={{zIndex: 1303}}>
                    {
                        this.state.chatInfo ? (
                            <div className={classes.paper}>
                                <GroupChatInfo handleMenuClose={this.handleMenuClose}/>
                            </div>
                            ) : (
                                this.state.attachments ? (
                                    <div className={classes.paperAttachment}>
                                        <AttachmentsModal handleMenuClose={this.handleMenuClose}/>
                                    </div>
                                    ) : (console.log('Чет ошибка какая-то, как ты это сделал?'))
                            )
                    }
                </Modal>
            </div>
        )
    }
}

export default withStyles(styles, {withTheme: true, index: 1})(ChatBar);