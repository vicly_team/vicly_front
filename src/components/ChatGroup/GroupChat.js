import React from 'react';
import Grid from '@material-ui/core/Grid/index';
import Avatar from '@material-ui/core/Avatar/index';
import Typography from '@material-ui/core/Typography/index';
import 'typeface-roboto';
import ListItem from "@material-ui/core/ListItem/ListItem";
import withStyles from "@material-ui/core/es/styles/withStyles";
import Badge from "@material-ui/core/Badge/Badge";
import {observer} from "mobx-react";
import {withRouter} from "react-router-dom";
import rootStore from "../../store/RootStore";
import AvatarColor from "../../services/AvatarColor"
import Group from "@material-ui/icons/Group";

const {accountStore, messagesStore} = rootStore;

const styles = theme => ({
    fixWidth: {
        margin: 0,
        width: 'inherit',
    },
    avatar: {
        width: 45,
        height: 45,
        borderRadius: 5,
    },
    listItemPadding: {
        padding: 'unset',
        borderRadius: 5,
    },
    margin: {
        top: 41,
        right: 17,
        backgroundColor: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.lightBadge : theme.palette.secondary.dark
            }`,
        color: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.dark : theme.palette.secondary.light
            }`,
        zIndex: 0,
    },
    fixPadding: {
        padding: 0,
        margin: 0,
    },
    contentPadding: {
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 9,
    },
    userName: {
        fontSize: '1rem',
        color: ` ${
            theme.palette.type === 'light' ? theme.palette.secondary.light : theme.palette.secondary.dark
            }`,
    },
    message: {
        color: ` ${
            theme.palette.type === 'light' ? '#adacac' : theme.palette.secondary.dark
            }`,
        fontSize: '0.9rem'
    },
    time: {
        color: ` ${
            theme.palette.type === 'light' ? '#adacac' : theme.palette.secondary.dark
            }`,

    },
    dialogIco: {
        color: ` ${
            theme.palette.type === 'light' ? '#adadad' : theme.palette.secondary.dark
            }`,
        marginLeft: 9,
    },
    chatTitleWithIcon: {
        display: 'flex',
        alignItems: 'flex-end',
    },
});

@observer
class GroupChat extends React.Component {

    constructor(props) {
        super(props);
        this.messagesStore = messagesStore;
        this.chatsStore = messagesStore;
    }

    handleDialogClick = () => {
        this.props.history.push(`/home/chat/group/${this.props.groupChat.chatId}`);
        this.props.handleDrawerToggleForMob();
    };

    handleDialogClickMob = () => {
        this.props.history.push(`/home/chat/group/${this.props.groupChat.chatId}`);
        this.props.handleDrawerToggle();
    };

    formatDate = (timestamp) => {
        const now = new Date(Date.now());
        let date = new Date(timestamp);
        const today = now.toDateString() == date.toDateString();
        const mins = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        if (today) {
            return date.getHours() + ":" + mins;
        } else {
            //  return date.getHours() + ":" + mins + " " + date.getDay() + "/" + date.getMonth() + "/" + (date.getFullYear() - 2000);
            return date.getDay() + "/" + date.getMonth() + "/" + (date.getFullYear() - 2000);
        }
    };

    render() {
        const {classes} = this.props;
        const {chatId, title: chatTitle, last, unread: countUnread} = this.props.groupChat;
        let lastMessageUser = null;
        let lastMessage = "";
        let lastMessageDatetime = null;
        const selected = this.props.groupChat.selected;
        if (last){
            lastMessage = last.message;
            lastMessageDatetime = last.timestamp_post.timestamp;
            if (last) {
                const user = rootStore.messagesStore.findUserByIdNew(last.from);
                lastMessageUser = user ? user.first_name : "error";
            }
        }


        let colorChange = AvatarColor.getColor(chatTitle[0]);
        return (

                    <ListItem
                        selected={selected}
                        onClick={this.handleDialogClick}
                        disableGutters={true}
                        button
                        className={classes.listItemPadding}>
                        <Grid container className={`${classes.fixWidth} ${selected ? classes.selected : ""}`}
                              wrap="nowrap"
                              spacing={2}>
                            <Grid item>
                                <Avatar
                                    className={classes.avatar} style={{backgroundColor: `${colorChange}`}}>
                                    {chatTitle[0].toUpperCase()}
                                </Avatar>
                            </Grid>

                            <Grid item xs zeroMinWidth>
                                <div className={classes.chatTitleWithIcon}>
                                    <Typography variant="body2"
                                                color="secondary"
                                                noWrap
                                                className={classes.userName}
                                                style={{color: selected ? '#fff' : ''}}>
                                        {chatTitle}
                                    </Typography>
                                    <Group className={classes.dialogIco}
                                           style={{color: selected ? '#b5dcdc' : ''}}/>
                                </div>

                                <Typography variant="caption"
                                            noWrap
                                            className={classes.message}
                                            style={{color: selected ? '#b5dcdc' : ''}}>
                                    {lastMessage ? lastMessage : "Нет сообщений"}
                                </Typography>
                            </Grid>

                            <Grid item>
                                <Typography
                                    className={classes.time}>{lastMessageDatetime ? this.formatDate(lastMessageDatetime) : ""}</Typography>
                            </Grid>
                            {
                                countUnread ? (<Badge badgeContent={countUnread} classes={{badge: classes.margin}}/>) : ("")
                            }
                        </Grid>
                    </ListItem>
        );
    }
}

const styledComponent = withStyles(styles, {withTheme: true, index: 1})(withRouter(GroupChat));

export default styledComponent;
